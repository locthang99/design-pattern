﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace SingletonDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //var obj1 = SingletonObj.GetInstance_LazyLoad();
            //var obj2 = SingletonObj.GetInstance_LazyLoad();
            //var obj2 = SingletonObj.GetInstance_ThreadSafe();

            //if (obj1.Equals(obj2))
            //{
            //    Console.WriteLine("Obj1=Obj2");
            //    Console.WriteLine("Id Obj1:"+obj1.Id);
            //    Console.WriteLine("Id Obj2:" + obj2.Id);
            //}

            //Console.WriteLine(SingletonObj.GetInstance_LazyLoad().Id);

            //SingletonObj.GetInstance_LazyLoad().IncreaseID();
            //obj1.IncreaseID();
            //obj2.IncreaseID();


            //Neu khong su dung ThreadSafe tao ra cac instance khac nhau
            Parallel.For(1, 4, i =>
            {
                Console.WriteLine("Id instance: " + SingletonObj.GetInstance_ThreadSafe().Id);
            });

            Console.ReadKey();
        }
        public static void Get()
        {          
               // SingletonObj.GetInstance_ThreadSafe().IncreaseID();
                Console.WriteLine(SingletonObj.GetInstance_LazyLoad().Id);         
        }
        

    }
}
