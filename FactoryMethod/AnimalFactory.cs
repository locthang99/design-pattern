﻿using System;
using System.Collections.Generic;
using System.Text;
using Product;
using Product.Products;

namespace FactoryMethod
{
    public class AnimalFactory
    {
        public static Animal GetAnimal(string Type)
        {
            if (Type.Equals("Bird"))
            {
                return new Bird();
            }
            else if (Type.Equals("Butterfly"))
            {
                return new Butterfly();
            }
            else
                 if (Type.Equals("Dog"))
            {
                return new Dog();
            }
            else if (Type.Equals("Cat"))
            {
                return new Cat();
            }
            return null;
        }
    }
}
