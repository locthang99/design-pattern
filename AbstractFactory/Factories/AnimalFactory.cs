﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactory
{
    public abstract class AnimalFactory
    {
        public abstract Animal GetAnimal(string AnimalType);
        public static AnimalFactory CreateAnimalFactory(string FactoryType)
        {
            if (FactoryType.Equals("Air"))
                return new AirAnimalFactory();
            else
                return new LandAnimalFactory();
        }
    }
}
