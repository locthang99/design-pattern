﻿using AbstractFactory.Products;

namespace AbstractFactory
{
    public class AirAnimalFactory : AnimalFactory
    {
        public override Animal GetAnimal(string AnimalType)
        {
            if (AnimalType.Equals("Bird"))
            {
                return new Bird();
            }
            else if (AnimalType.Equals("Butterfly"))
            {
                return new Butterfly();
            }
            else
                return null;
        }
    }
}