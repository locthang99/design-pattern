﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactory.Products
{
    public class Dog: Animal
    {
        public string Speak()
        {
            return "Gau gau gau...";
        }
    }
}
