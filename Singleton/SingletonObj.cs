﻿using System;
namespace Singleton
{
    public class SingletonObj
    {
        private static SingletonObj Instance;
        private static readonly object PadLock = new object();
        //private static SingletonObj Instance = new SingletonObj();//khoi tao theo kieu Eager

        public int Id { get; set; }
        private SingletonObj()
        {           
            Id= 0;
        }
        public void IncreaseID()
        {
            Id++;
        }

        //cac method lay ra instance cua class
        public static SingletonObj GetInstance_Eager()
        {
            return Instance;
        }

        public static SingletonObj GetInstance_LazyLoad()
        {
            if (Instance == null)
                Instance = new SingletonObj();
            return Instance;
        }

        public static SingletonObj GetInstance_ThreadSafe()
        {
            lock (PadLock)
            {
                if (Instance == null)
                    Instance = new SingletonObj();
                return Instance;
            }
        }

        public static SingletonObj GetInstance_ThreadSafeDoubleCheck()
        {

            if (Instance == null)
            {
                lock (PadLock)
                {
                    if (Instance == null)
                        Instance = new SingletonObj();
                }
            }
            return Instance;

        }
    }
}
